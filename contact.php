<?php
require_once "include/header.php";

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["send"] == "Send") {
    $message = trim($_POST["message"]);
    $name = trim($_POST["name"]);
    $email = trim($_POST["email"]);
    $subject = trim($_POST["subject"]);

    if (empty($message)) {
        $errors["message"] = "Please write a message.";
    }

    if (empty($name)) {
        $errors["name"] = "Please enter name.";
    }

    if (empty($email)) {
        $errors["email"] = "Please enter email.";
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors["email"] = "Please enter valid email.";
    }

    if (empty($subject)) {
        $errors["subject"] = "Please enter subject.";
    }

    if (empty($errors)) {
        $date = date('Y-m-d H:i:s');

        $insertQuery = "INSERT INTO contact (name, email, message, subject, createdAt)
        VALUES ('$name', '$email', '$message', '$subject', '$date')
        ";
        
        $insertResult = mysqli_query($connection, $insertQuery);

        if ($insertResult) {
            $showSuccess = true;
        } else {
            $showError = true;
        }
    } else {
        $hasError = true;
    }
}
?>

<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Contact us</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Contact</a></li>
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--?  Contact Area start  -->
    <section class="contact-section">
        <div class="container">
            <?php
                if ($showSuccess) {
                    echo '<div class="alert alert-success" role="alert">
                            Message Saved
                        </div>';
                } 

                if ($showError) {
                    echo '<div class="alert alert-danger" role="alert">
                        Failed to create message!
                    </div>';
                }
                ?>


            <div class="d-none d-sm-block mb-5 pb-4">
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title">Get in Touch</h2>
                </div>



                <div class="col-lg-8">
                    <form class="form-contact <?php echo $hasError ? ' needs-validation was-validated' : '';?>"
                        action="" method="POST" novalidate>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" name="message" id="message" cols="30"
                                        rows="9" onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Enter Message'" placeholder=" Enter Message"
                                        required></textarea>
                                    <span class="invalid-feedback"><?php echo $errors['message'];?></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="name" type="text"
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'"
                                        placeholder="Enter your name" required>
                                    <span class="invalid-feedback"><?php echo $errors['name'];?></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control valid" name="email" id="email" type="email"
                                        onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Enter email address'" placeholder="Email" required>
                                    <span class="invalid-feedback"><?php echo $errors['email'];?></span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="subject" id="subject" type="text"
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'"
                                        placeholder="Enter Subject" required>
                                    <span class="invalid-feedback"><?php echo $errors['subject'];?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" name="send" value="Send"
                                class="button button-contactForm boxed-btn">Send</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- Contact Area End -->
</main>

<?php
require_once "include/footer.php"
?>