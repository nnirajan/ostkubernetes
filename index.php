<?php 
require_once "include/header.php";
?>
<main>
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Kubernetes</h1>
                                <p data-animation="fadeInLeft" data-delay="0.4s">
                                    <a href="#">Kubernetes</a>, also known as K8s, is an open-source
                                    system for automating deployment, scaling, and management of containerized
                                    applications.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Features -->
    <div class="courses-area section-padding40 fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8">
                    <div class="section-tittle text-center mb-55">
                        <h2>Kubernetes Features</h2>
                    </div>
                </div>
            </div>

            <div class="row">

                <?php
$featuresSQL = "SELECT id, title, description from features";

$featuresQuery = mysqli_query($connection, $featuresSQL);

if ($featuresQuery) {
    while ($feature = mysqli_fetch_assoc($featuresQuery)) {
?>

                <div class="col-lg-4">
                    <div class="properties properties2 mb-30">
                        <div class="properties__card">
                            <!-- <div class="properties__img overlay1">
                            <a href="#"><img src="assets/img/gallery/featured1.png" alt=""></a>
                        </div> -->
                            <div class="properties__caption">
                                <h3><?php echo $feature["title"];?></h3>
                                <p>
                                    <?php echo $feature["description"];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
    }   
}
?>
            </div>
        </div>
    </div>
    <!-- Features End -->

    <!-- Components -->
    <div class="courses-area fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8">
                    <div class="section-tittle text-center mb-55">
                        <h2>Kubernetes Components</h2>
                    </div>
                </div>
            </div>

            <?php
                $componentsSQL = "SELECT id, title, description from components";

                $componentsQuery = mysqli_query($connection, $componentsSQL);

                if ($componentsQuery) {
                    while ($component = mysqli_fetch_assoc($componentsQuery)) {
                        $componentId = $component["id"];
                        $componentTitle = $component["title"];
                        $componentDescription = $component["description"];

                        if ($componentId % 2 == 1) {
                ?>

            <!-- left -->
            <div class="section-top-border">
                <h2><?php echo $componentTitle;?></h2>
                <div class="row">
                    <!-- <div class="col-md-3">
                            <img src="assets/img/elements/d.jpg" alt="" class="img-fluid">
                        </div> -->
                    <div class="col-md-9 mt-sm-20">
                        <p>
                            <?php echo $componentDescription;?>
                        </p>
                    </div>
                </div>
            </div>

            <?php
                } else {
            ?>

            <!-- right -->
            <div class="section-top-border text-right">
                <h2><?php echo $componentTitle; ?></h2>
                <div class="row">
                    <div class="col-md-3">
                        <!-- empty div to push right -->
                    </div>
                    <div class="col-md-9 mt-sm-20">
                        <p class="text-right">
                            <?php echo $componentDescription; ?>
                        </p>
                    </div>
                    <!-- <div class="col-md-3">
                            <img src="assets/img/elements/d.jpg" alt="" class="img-fluid">
                        </div> -->
                </div>
            </div>

            <?php
                            
                        }
                        ?>

            <?php
                    }
                }

                ?>

            <!-- Components Area -->

        </div>
        <!-- Components End -->
</main>

<?php 
require_once "include/footer.php";
?>