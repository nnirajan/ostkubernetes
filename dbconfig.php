<?php
session_start();

define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_TABLE', 'blog');

$connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_TABLE);

if (!$connection) {
    die("Error!!! Cannnot connect to DB. ". mysqli_connect_error());
}

// $dbServer = "localhost";
// $username = "root";
// $dbPassword = "root";
// $table = "blog";

// $connection = mysqli_connect($dbServer, $username, $dbPassword, $table) or die(mysqli_connect_error());
?>