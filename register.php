<?php
require_once "dbconfig.php";

$errors = [];
$hasError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["register"] == "Registration") {
    $name = trim($_POST['name']);
    $email = trim($_POST["email"]);
    $password = $_POST["password"];
    $confirmPassword = $_POST["confirmPassword"];

    if (empty($name)) {
        $errors["name"] = "Please enter name.";
    }

    if (empty($email)) {
        $errors["email"] = "Please enter email.";
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors["email"] = "Please enter valid email.";
    } else {
        $selectQuery = "SELECT email from users where email = '$email'";
        $selectResult = mysqli_query($connection, $selectQuery);

        $num = mysqli_num_rows($selectResult);

        if ($num>0) {
            $errors["email"] = "Please enter unqiue email.";
        }
    }

    if (empty($password)) {
        $errors["password"] = "Please enter password.";
    } else {
        if (empty($confirmPassword)) {
            $errors["confirmPassword"] = "Please enter confirm password.";
        } else {
            if ($password != $confirmPassword) {
                $errors["confirmPassword"] = "Confirm password does not match with password.";
            }
        }
    }

    if (empty($errors)) {
        $hasError = false;

        $insertSQL = "INSERT INTO users (`name`, `email`, `password`) 
        VALUES ('$name', '$email', '$password')";

        $insertResult = mysqli_query($connection, $insertSQL);

        if ($insertResult) {
            $showSuccess = true;
        } else {
            $showError = true;
        }
    } else {
        $hasError = true;
    }
}
?>

<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Kubernetes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/progressbar_barfiller.css">
    <link rel="stylesheet" href="assets/css/gijgo.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/animated-headline.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="assets/img/logo/loder.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start-->

    <!-- Register -->
    <main class="login-body" data-vide-bg="assets/img/login-bg.mp4">
        <!-- Registration Form -->
        <form action="" method="POST" novalidate>
            <div class="login-form <?php echo $hasError ? ' needs-validation was-validated' : '';?>">

                <!-- logo-login -->
                <div class="logo-login">
                    <a href="index.php"><img src="assets/img/logo/loder.png" alt=""></a>
                </div>
                <h2>Registration Here</h2>

                <?php
                if ($showSuccess) {
                    echo '<div class="alert alert-success" role="alert">
                            User created!
                        </div>';
                } 

                if ($showError) {
                    echo '<div class="alert alert-danger" role="alert">
                        Failed to create User!
                    </div>';
                }
                ?>

                <div class="form-input">
                    <label for="name">Full name</label>
                    <input type="text" name="name" placeholder="Full name" class="form-control" required>
                    <span class="invalid-feedback"><?php echo $errors['name'];?></span>
                </div>

                <div class="form-input">
                    <label for="name">Email Address</label>
                    <input type="email" name="email" placeholder="Email Address" class="form-control" required>
                    <span class="invalid-feedback"><?php echo $errors['email'];?></span>

                </div>

                <div class="form-input">
                    <label for="name">Password</label>
                    <input type="password" name="password" placeholder="Password" class="form-control" required>
                    <span class="invalid-feedback"><?php echo $errors['password'];?></span>
                </div>

                <div class="form-input">
                    <label for="name">Confirm Password</label>
                    <input type="password" name="confirmPassword" placeholder="Confirm Password" class="form-control"
                        required>
                    <span class="invalid-feedback"><?php echo $errors['confirmPassword'];?></span>
                </div>

                <div class="form-input pt-30">
                    <input type="submit" name="register" value="Registration">
                </div>

                <!-- Login -->
                <a href="login.php" class="registration">Login</a>
            </div>
        </form>
        <!-- /End Registration Form -->
    </main>

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Video bg -->
    <script src="./assets/js/jquery.vide.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- Date Picker -->
    <script src="./assets/js/gijgo.min.js"></script>
    <!-- Nice-select, sticky -->
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <!-- Progress -->
    <script src="./assets/js/jquery.barfiller.js"></script>

    <!-- counter , waypoint,Hover Direction -->
    <script src="./assets/js/jquery.counterup.min.js"></script>
    <script src="./assets/js/waypoints.min.js"></script>
    <script src="./assets/js/jquery.countdown.min.js"></script>
    <script src="./assets/js/hover-direction-snake.min.js"></script>

    <!-- contact js -->
    <!-- <script src="./assets/js/contact.js"></script> -->
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>

</body>

</html>