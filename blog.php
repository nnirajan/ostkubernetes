<?php
require_once "include/header.php";

$userId = $_SESSION['userId'];

// post blog
$errors = [];
$hasError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["blog"] == "Blog") {
    $detail = trim($_POST["detail"]);

    if (empty($detail)) {
        $errors["detail"] = "Please write a blog.";
    }

    if (empty($errors)) {
        $date = date('Y-m-d H:i:s');

        $insertQuery = "INSERT INTO blogs (detail, createdAt, userId)
        VALUES ('$detail', '$date', '$userId')
        ";
        
        $insertResult = mysqli_query($connection, $insertQuery);

        if ($insertResult) {
            $showSuccess = true;
        } else {
            $showError = true;
        }
    } else {
        $hasError = true;
    }
}

// fetch blogs
$blogsQuery = "SELECT b.id, detail, createdAt, u.name FROM blogs AS b
INNER JOIN users AS u
ON u.id = b.userId
ORDER BY createdAt DESC
";

$blogsResult = mysqli_query($connection, $blogsQuery);

?>

<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Blog</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item"><a href="#">Blog</a></li>
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--? Blog Area Start -->
    <section class="blog_area single-post-area section-padding">
        <div class="container">
            <div class="row">

                <?php
                if(isset($userId)) {
                ?>

                <div class="col-lg-10">
                    <h2>Write a Blog</h2>
                    <form class="form-contact <?php echo $hasError ? ' needs-validation was-validated' : '';?>"
                        action="" method="POST" novalidate>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="detail" id="detail" cols="40" rows="5"
                                        placeholder="Write a Blog" required></textarea>
                                    <span class="invalid-feedback"><?php echo $errors['detail'];?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="blog" value="Blog"
                                class="button button-contactForm btn_1 boxed-btn">Post</button>
                        </div>
                    </form>
                </div>

                <?php
                } else {
                ?>

                <div class="col-lg-10">
                    <h2>Log In To Write Blog</h2>

                    <a href="login.php"><button type="submit" name="login" value="Login"
                            class="button button-contactForm btn_1 boxed-btn">Login</button></a>
                </div>
            </div>

            <?php
                }
                ?>

            <?php
                    if($blogsResult) {
                        while($blog = mysqli_fetch_assoc($blogsResult)) {
                ?>

            <div class="col-lg-10 posts-list">
                <div class="comments-area">
                    <div class="comment-list">
                        <div class="single-comment justify-content-between d-flex">
                            <div class="user justify-content-between d-flex">
                                <div class="thumb">
                                    <!-- <img src="assets/img/blog/comment_1.png" alt=""> -->
                                </div>
                                <div class="desc">
                                    <p class="comment">
                                        <?php
                                                echo $blog['detail'];
                                            ?>
                                    </p>
                                    <div class="d-flex justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <h5>
                                                <a>
                                                    <?php
                                                        echo $blog['name'];
                                                        ?>
                                                </a>
                                            </h5>
                                            <p class="date"><?php echo $blog['createdAt'];?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
                        }
                    }
                ?>

        </div>
        </div>
    </section>
    <!-- Blog Area End -->
</main>

<?php require_once "include/footer.php"?>